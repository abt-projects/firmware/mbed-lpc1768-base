/*******************************************************************************
 * Title      : Build Timestamp
 * Project    : 
 *******************************************************************************
 * File       : timestamp.c
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 23/04/2019
 * Last update: 
 * Platform   : 
 * Standard   : ANSI C
 *******************************************************************************
 * Description: 
 *******************************************************************************
 * Copyright (c) 2019 CERN
 *******************************************************************************
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 23/04/2019  1.0      Lea Strobino  Created
 ******************************************************************************/

#include "timestamp.h"

const char __BUILD_TIMESTAMP__[] = __DATE__" "__TIME__;

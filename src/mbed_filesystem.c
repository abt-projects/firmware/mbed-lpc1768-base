/*******************************************************************************
 * Title      : mbed file system access via semihosting
 * Project    : 
 *******************************************************************************
 * File       : mbed_filesystem.c
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 24/08/2020
 * Last update: 05/05/2022
 * Platform   : 
 * Standard   : ANSI C
 *******************************************************************************
 * Description: 
 *******************************************************************************
 * Copyright (c) 2020 CERN
 *******************************************************************************
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 24/08/2020  1.0      Lea Strobino  Created
 * 05/05/2022  1.1      Lea Strobino  Added function to read mbed mac address
 ******************************************************************************/

#include <string.h>

#include "mbed_filesystem.h"

#define SYS_OPEN                0x01
#define SYS_CLOSE               0x02
#define SYS_WRITE               0x05
#define SYS_READ                0x06
#define SYS_SEEK                0x0A
#define SYS_FLEN                0x0C

#define MODE_R                  0x00
#define MODE_PLUS               0x02
#define MODE_W                  0x04
#define MODE_A                  0x08

#define USR_UID                 0x101
#define DEVICE_ID_LENGTH        32
#define DEVICE_MAC_OFFSET       20

int32_t mbed_fs_open(FILE *fp, const char *filename, uint32_t flags) {

  uint32_t mode = 0;
  if (flags & O_RDWR) {
    mode = MODE_PLUS;
    if (flags & O_APPEND) {
      mode |= MODE_A;
    } else if (flags & O_TRUNC) {
      mode |= MODE_W;
    } else {
      mode |= MODE_R;
    }
  } else if (flags & O_WRONLY) {
    if (flags & O_APPEND) {
      mode = MODE_A;
    } else {
      mode = MODE_W;
    }
  } else if (flags == O_RDONLY) {
    mode = MODE_R;
  } else {
    return EOF;
  }

  uint32_t args[3];
  args[0] = (uint32_t)filename;
  args[1] = (uint32_t)mode;
  args[2] = (uint32_t)strlen(filename);

  int32_t handle = __semihost(SYS_OPEN, args);

  if (handle != -1) {
    fp->handle = handle;
    fp->offset = 0;
    return 0;
  }

  return EOF;

}

int32_t mbed_fs_close(FILE *fp) {
  return __semihost(SYS_CLOSE, &fp->handle) ? EOF : 0;
}

size_t mbed_fs_read(FILE *fp, char *buf, size_t count) {

  if (count == 0) return 0;

  uint32_t args[3];
  args[0] = (uint32_t)fp->handle;
  args[1] = (uint32_t)buf;
  args[2] = (uint32_t)count;

  count -= __semihost(SYS_READ, args);
  fp->offset += count;

  return count;

}

size_t mbed_fs_write(FILE *fp, const char *buf, size_t count) {

  if (count == 0) return 0;

  uint32_t args[3];
  args[0] = (uint32_t)fp->handle;
  args[1] = (uint32_t)buf;
  args[2] = (uint32_t)count;

  count -= __semihost(SYS_WRITE, args);
  fp->offset += count;

  return count;

}

int32_t mbed_fs_seek(FILE *fp, int32_t offset, uint32_t whence) {

  int32_t flen;
  switch (whence) {
    case SEEK_SET:
      break;
    case SEEK_CUR:
      offset += fp->offset;
      break;
    case SEEK_END:
      flen = __semihost(SYS_FLEN, &fp->handle);
      if (flen == -1) return EOF;
      offset += flen;
      break;
    default:
      return EOF;
  }

  uint32_t args[2];
  args[0] = (uint32_t)fp->handle;
  args[1] = (uint32_t)offset;

  if (__semihost(SYS_SEEK, args) == 0) {
    fp->offset = offset;
    return offset;
  }

  return EOF;

}

int32_t mbed_read_mac_address(uint8_t *mac) {

  char uid[DEVICE_ID_LENGTH+1];

  uint32_t args[2];
  args[0] = (uint32_t)uid;
  args[1] = sizeof(uid);

  if (__semihost(USR_UID, args) == 0) {

    for (uint32_t byte, i=0; i<6; i++) {
      sscanf(&uid[DEVICE_MAC_OFFSET+2*i], "%2X", &byte);
      mac[i] = byte;
    }

    mac[0] &= ~0x01;
    return 0;

  }

  return EOF;

}

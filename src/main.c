/*******************************************************************************
 * Title      : 
 * Project    : 
 *******************************************************************************
 * File       : main.c
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 13/04/2022
 * Last update: 
 * Platform   : 
 * Standard   : ANSI C
 *******************************************************************************
 * Description: 
 *******************************************************************************
 * Copyright (c) 2020 CERN
 *******************************************************************************
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 13/04/2022  1.0      Lea Strobino  Created
 ******************************************************************************/

#include "mbed_LPC1768.h"

#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "uart_stdio.h"

#include "timestamp.h"

__declspec(noreturn) int32_t main() {

  // Configure mbed LPC1768 pins
  GPIO_SetDir(MBED_ETH_OSC_EN_PORT,  MBED_ETH_OSC_EN_BIT,  GPIO_DIR_OUTPUT);
  GPIO_SetDir(MBED_ETH_RESET_N_PORT, MBED_ETH_RESET_N_BIT, GPIO_DIR_OUTPUT);
  GPIO_SetDir(MBED_LED_PORT,         MBED_LED1_BIT,        GPIO_DIR_OUTPUT);
  GPIO_SetDir(MBED_LED_PORT,         MBED_LED2_BIT,        GPIO_DIR_OUTPUT);
  GPIO_SetDir(MBED_LED_PORT,         MBED_LED3_BIT,        GPIO_DIR_OUTPUT);
  GPIO_SetDir(MBED_LED_PORT,         MBED_LED4_BIT,        GPIO_DIR_OUTPUT);

  // Configure USB UART
  stdio_init();

  printf("\n\e[2J\e[H"
    "************************************************\n"
    "*        CERN Beam Transfer Electronics        *\n"
    "*                 mbed LPC1768                 *\n"
    "************************************************\n\n"
  );

  printf("Build date: %s\n\n", __BUILD_TIMESTAMP__);

  uint32_t i;

  while (1) {
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED1_BIT, 1);
    for (i = 0; i < (1 << 24); i++);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED1_BIT, 0);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED2_BIT, 1);
    for (i = 0; i < (1 << 24); i++);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED2_BIT, 0);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED3_BIT, 1);
    for (i = 0; i < (1 << 24); i++);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED3_BIT, 0);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED4_BIT, 1);
    for (i = 0; i < (1 << 24); i++);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED4_BIT, 0);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED3_BIT, 1);
    for (i = 0; i < (1 << 24); i++);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED3_BIT, 0);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED2_BIT, 1);
    for (i = 0; i < (1 << 24); i++);
    GPIO_PinWrite(MBED_LED_PORT, MBED_LED2_BIT, 0);
  }

}

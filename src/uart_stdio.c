/*******************************************************************************
 * Title      : UART stdin/stdout handler
 * Project    : 
 *******************************************************************************
 * File       : uart_stdio.c
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 17/07/2020
 * Last update: 05/08/2022
 * Platform   : 
 * Standard   : ANSI C
 *******************************************************************************
 * Description: http://www.keil.com/support/man/docs/armlib/armlib_chr1358938940288.htm
 *******************************************************************************
 * Copyright (c) 2020 CERN
 *******************************************************************************
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 05/08/2022  1.0      Lea Strobino  Created
 ******************************************************************************/

#include "uart_stdio.h"

#include "PIN_LPC17xx.h"
#include "UART_LPC17xx.h"

// Peripheral
#define UART                    LPC_UART0
#define UART_RX_EN              RTE_UART0_RX_PIN_EN
#define UART_RX_PORT            RTE_UART0_RX_PORT
#define UART_RX_BIT             RTE_UART0_RX_BIT
#define UART_RX_FUNC            RTE_UART0_RX_FUNC
#define UART_TX_EN              RTE_UART0_TX_PIN_EN
#define UART_TX_PORT            RTE_UART0_TX_PORT
#define UART_TX_BIT             RTE_UART0_TX_BIT
#define UART_TX_FUNC            RTE_UART0_TX_FUNC

// Baud rate: PCLK / (16 * (256*DLM + DLL) * (1 + DIVADDVAL/MULVAL))
//            100 MHz / (16 * 31 * (1 + 3/4)) = 115200 bit/s
#define UART_DLM                0
#define UART_DLL                31
#define UART_DIVADDVAL          3
#define UART_MULVAL             4

#if (UART_RX_EN && defined(RTE_Compiler_IO_STDIN_User)) || (UART_TX_EN && defined(RTE_Compiler_IO_STDOUT_User))

void stdio_init() {
  // Configure baud rate
  UART->LCR = USART_LCR_DLAB;
  UART->DLM = UART_DLM;
  UART->DLL = UART_DLL;
  UART->FDR = (UART_DIVADDVAL << USART_FDR_DIVADDVAL_POS) | (UART_MULVAL << USART_FDR_MULVAL_POS);
  // Configure data format: 8 data bits, 1 stop bit, no parity
  UART->LCR = 0b11;
  // Configure pins
#if UART_RX_EN
  PIN_Configure(UART_RX_PORT, UART_RX_BIT, UART_RX_FUNC, PIN_PINMODE_PULLUP, PIN_PINMODE_NORMAL);
#endif
#if UART_TX_EN
  PIN_Configure(UART_TX_PORT, UART_TX_BIT, UART_TX_FUNC, PIN_PINMODE_PULLUP, PIN_PINMODE_NORMAL);
#endif
  // Enable FIFOs
  UART->FCR = USART_FCR_FIFOEN;
}

#endif

#if UART_RX_EN && defined(RTE_Compiler_IO_STDIN_User)

static int32_t fgetc_fifo = EOF;
static uint8_t fgetc_undo = 0;

int32_t fgetc(FILE *stream) {
  if (fgetc_undo) {
    // Return the last byte that was read
    int32_t c = fgetc_fifo;
    fgetc_fifo = EOF;
    fgetc_undo = 0;
    return c;
  }
  // Wait for RX FIFO valid
  while (!(UART->LSR & USART_LSR_RDR));
  // Read one byte
  return fgetc_fifo = UART->RBR;
}

// Used by the scanf family of functions
int32_t __backspace(FILE *stream) {
  if (fgetc_fifo != EOF) {
    fgetc_undo = 1;
    return 0;
  }
  return EOF;
}

uint32_t stdin_fifo_valid() {
  return fgetc_undo || (UART->LSR & USART_LSR_RDR);
}

// Not used, referred from weak fgetc in retarget_io.c
int32_t stdin_getchar() {
  return EOF;
}

#endif

#if UART_TX_EN && defined(RTE_Compiler_IO_STDOUT_User)

int32_t fputc(int32_t c, FILE *stream) {
  // Wait for TX FIFO not full
  while (!(UART->LSR & USART_LSR_THRE));
  // Write one byte
  UART->THR = c;
  return c;
}

// Not used, referred from weak fputc in retarget_io.c
int32_t stdout_putchar(int32_t c) {
  return EOF;
}

#endif

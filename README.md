# mbed LPC1768 base project

Keil µVision base project for mbed LPC1768, based on [Keil.LPC1700_DFP.2.7.1.pack](https://www.keil.com/dd2/pack/#Keil.LPC1700_DFP) device support package.

<img src="//gitlab.cern.ch/uploads/-/system/project/avatar/99293/mbed-lpc1768.jpg" width="275">

## What is included

- Header file containing mbed LPC1768 pin configuration: [mbed_LPC1768.h](include/mbed_LPC1768.h).  
  This file should be used in every source file in place of `LPC17xx.h`.

- Lightweight UART driver: [uart_stdio.c](src/uart_stdio.c).  
  Enables the use of `printf` and `scanf` functions on the USB UART comprised in the interface chip.

- Minimal file system driver: [mbed_filesystem.c](src/mbed_filesystem.c).  
  Gives access to files stored on the mbed USB drive, with a few restrictions:
  - File access calls will block, including interrupts, as [semihosting](https://www.keil.com/support/man/docs/armcc/armcc_pge1358787048379.htm) uses the `BKPT` instruction.
  - Only 8.3 filenames are supported, sub-directories are not supported.
  - <p>Seek is not supported for files opened for writing.</p>

- Test code for the UART and the embedded LEDs in [main.c](src/main.c).

- Build configuration producing a `.bin` file (in `objects` folder) ready to be copied onto the mbed USB drive.

- Debug configuration for the CMSIS-DAP debugger comprised in the interface chip.

## Default configuration

- CPU clock at 100 MHz.
- USB UART at 115200 bit/s with 8 data bits, 1 stop bit, no parity.
- All peripherals off except UART0 and GPIOs.
- Ethernet PHY and external CMOS oscillator disabled.

## Documentation

- [Board schematics](//gitlab.cern.ch/abt-projects/firmware/mbed-lpc1768-base/-/raw/master/mbed/mbed LPC1768 %28mbed-005.1%29.pdf)
- [LPC1768 Datasheet](//gitlab.cern.ch/abt-projects/firmware/mbed-lpc1768-base/-/raw/master/mbed/LPC1768 Datasheet.pdf)
- [LPC1768 User manual](//gitlab.cern.ch/abt-projects/firmware/mbed-lpc1768-base/-/raw/master/mbed/LPC1768 User manual.pdf)
- [LPC1768 Errata sheet](//gitlab.cern.ch/abt-projects/firmware/mbed-lpc1768-base/-/raw/master/mbed/LPC1768 Errata sheet.pdf)
- [Documentation on mbed website](https://os.mbed.com/platforms/mbed-LPC1768/)

## Interface firmware & driver

- [mbed interface firmware (rev 141212)](//gitlab.cern.ch/abt-projects/firmware/mbed-lpc1768-base/-/raw/master/mbed/mbed_interface_firmware_141212.if)
- [Windows serial port driver (rev 16466)](//gitlab.cern.ch/abt-projects/firmware/mbed-lpc1768-base/-/raw/master/mbed/mbed_serial_driver_16466.exe)

## License

This project is licensed under [CERN-OHL-W v2](LICENSE), except files in the 'mbed' folder, which are included here for convenience only.

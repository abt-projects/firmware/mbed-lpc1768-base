/*******************************************************************************
 * Title      : mbed LPC1768 pin configuration
 * Project    : 
 *******************************************************************************
 * File       : mbed_LPC1768.h
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 17/07/2020
 * Last update: 13/04/2022
 * Platform   : 
 * Standard   : ANSI C
 *******************************************************************************
 * Description: 
 *******************************************************************************
 * Copyright (c) 2020 CERN
 *******************************************************************************
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 13/04/2022  1.0      Lea Strobino  Created
 ******************************************************************************/

#ifndef MBED_LPC1768_H_
#define MBED_LPC1768_H_

#include <stdint.h>

#define __NO_EMBEDDED_ASM
#include "LPC17xx.h"

#if defined(__CC_ARM)

#pragma import(__use_no_semihosting_swi)

#define __REV16(value) ({ \
  uint32_t __value = (value); \
  ((__value & 0x00FF00FF) << 8) | ((__value & 0xFF00FF00) >> 8); \
})

typedef struct {
  uint32_t bit0  : 1;
  uint32_t bit1  : 1;
  uint32_t bit2  : 1;
  uint32_t bit3  : 1;
  uint32_t bit4  : 1;
  uint32_t bit5  : 1;
  uint32_t bit6  : 1;
  uint32_t bit7  : 1;
} bit8_t __attribute__((bitband));

typedef struct {
  uint32_t bit0  : 1;
  uint32_t bit1  : 1;
  uint32_t bit2  : 1;
  uint32_t bit3  : 1;
  uint32_t bit4  : 1;
  uint32_t bit5  : 1;
  uint32_t bit6  : 1;
  uint32_t bit7  : 1;
  uint32_t bit8  : 1;
  uint32_t bit9  : 1;
  uint32_t bit10 : 1;
  uint32_t bit11 : 1;
  uint32_t bit12 : 1;
  uint32_t bit13 : 1;
  uint32_t bit14 : 1;
  uint32_t bit15 : 1;
} bit16_t __attribute__((bitband));

typedef struct {
  uint32_t bit0  : 1;
  uint32_t bit1  : 1;
  uint32_t bit2  : 1;
  uint32_t bit3  : 1;
  uint32_t bit4  : 1;
  uint32_t bit5  : 1;
  uint32_t bit6  : 1;
  uint32_t bit7  : 1;
  uint32_t bit8  : 1;
  uint32_t bit9  : 1;
  uint32_t bit10 : 1;
  uint32_t bit11 : 1;
  uint32_t bit12 : 1;
  uint32_t bit13 : 1;
  uint32_t bit14 : 1;
  uint32_t bit15 : 1;
  uint32_t bit16 : 1;
  uint32_t bit17 : 1;
  uint32_t bit18 : 1;
  uint32_t bit19 : 1;
  uint32_t bit20 : 1;
  uint32_t bit21 : 1;
  uint32_t bit22 : 1;
  uint32_t bit23 : 1;
  uint32_t bit24 : 1;
  uint32_t bit25 : 1;
  uint32_t bit26 : 1;
  uint32_t bit27 : 1;
  uint32_t bit28 : 1;
  uint32_t bit29 : 1;
  uint32_t bit30 : 1;
  uint32_t bit31 : 1;
} bit32_t __attribute__((bitband));

#endif

#include "RTE_Device.h"
#include "RTE_Components.h"

#define MBED_LED_PORT           1
#define MBED_LED1_BIT           18
#define MBED_LED2_BIT           20
#define MBED_LED3_BIT           21
#define MBED_LED4_BIT           23
#define MBED_LED_FUNC_GPIO      0
#define MBED_LED_FUNC_PWM       2

#define MBED_ETH_OSC_EN_PORT    1
#define MBED_ETH_OSC_EN_BIT     27
#define MBED_ETH_OSC_EN_FUNC    0

#define MBED_ETH_RESET_N_PORT   1
#define MBED_ETH_RESET_N_BIT    28
#define MBED_ETH_RESET_N_FUNC   0

#define MBED_ETH_LED_LINK_PORT  1
#define MBED_ETH_LED_LINK_BIT   25
#define MBED_ETH_LED_LINK_FUNC  0

#define MBED_ETH_LED_SPEED_PORT 1
#define MBED_ETH_LED_SPEED_BIT  26
#define MBED_ETH_LED_SPEED_FUNC 0

#define MBED_DIP5_PORT          0
#define MBED_DIP5_BIT           9

#define MBED_DIP6_PORT          0
#define MBED_DIP6_BIT           8

#define MBED_DIP7_PORT          0
#define MBED_DIP7_BIT           7

#define MBED_DIP8_PORT          0
#define MBED_DIP8_BIT           6

#define MBED_DIP9_PORT          0
#define MBED_DIP9_BIT           0

#define MBED_DIP10_PORT         0
#define MBED_DIP10_BIT          1

#define MBED_DIP11_PORT         0
#define MBED_DIP11_BIT          18

#define MBED_DIP12_PORT         0
#define MBED_DIP12_BIT          17

#define MBED_DIP13_PORT         0
#define MBED_DIP13_BIT          15

#define MBED_DIP14_PORT         0
#define MBED_DIP14_BIT          16

#define MBED_DIP15_PORT         0
#define MBED_DIP15_BIT          23

#define MBED_DIP16_PORT         0
#define MBED_DIP16_BIT          24

#define MBED_DIP17_PORT         0
#define MBED_DIP17_BIT          25

#define MBED_DIP18_PORT         0
#define MBED_DIP18_BIT          26

#define MBED_DIP19_PORT         1
#define MBED_DIP19_BIT          30

#define MBED_DIP20_PORT         1
#define MBED_DIP20_BIT          31

#define MBED_DIP21_PORT         2
#define MBED_DIP21_BIT          5

#define MBED_DIP22_PORT         2
#define MBED_DIP22_BIT          4

#define MBED_DIP23_PORT         2
#define MBED_DIP23_BIT          3

#define MBED_DIP24_PORT         2
#define MBED_DIP24_BIT          2

#define MBED_DIP25_PORT         2
#define MBED_DIP25_BIT          1

#define MBED_DIP26_PORT         2
#define MBED_DIP26_BIT          0

#define MBED_DIP27_PORT         0
#define MBED_DIP27_BIT          11

#define MBED_DIP28_PORT         0
#define MBED_DIP28_BIT          10

#define MBED_DIP29_PORT         0
#define MBED_DIP29_BIT          5

#define MBED_DIP30_PORT         0
#define MBED_DIP30_BIT          4

#define MBED_DIP31_PORT         0
#define MBED_DIP31_BIT          29

#define MBED_DIP32_PORT         0
#define MBED_DIP32_BIT          30

#endif

/*******************************************************************************
 * Title      : UART stdin/stdout handler
 * Project    : 
 *******************************************************************************
 * File       : uart_stdio.h
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 17/07/2020
 * Last update: 
 * Platform   : 
 * Standard   : ANSI C
 *******************************************************************************
 * Description: 
 *******************************************************************************
 * Copyright (c) 2020 CERN
 *******************************************************************************
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 17/07/2020  1.0      Lea Strobino  Created
 ******************************************************************************/

#ifndef UART_STDIO_H_
#define UART_STDIO_H_

#include <stdint.h>
#include <stdio.h>

#ifdef  __cplusplus
extern "C" {
#endif

void stdio_init(void);

int32_t  fgetc(FILE *stream);
int32_t  fputc(int32_t c, FILE *stream);
uint32_t stdin_fifo_valid(void);

#ifdef __cplusplus
}
#endif

#endif

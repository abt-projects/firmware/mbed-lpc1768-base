/*******************************************************************************
 * Title      : mbed file system access via semihosting
 * Project    : 
 *******************************************************************************
 * File       : mbed_filesystem.h
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 24/08/2020
 * Last update: 05/05/2022
 * Platform   : 
 * Standard   : ANSI C
 *******************************************************************************
 * Description: 
 *******************************************************************************
 * Copyright (c) 2020 CERN
 *******************************************************************************
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 24/08/2020  1.0      Lea Strobino  Created
 * 05/05/2022  1.1      Lea Strobino  Added function to read mbed mac address
 ******************************************************************************/

#ifndef MBED_FILESYSTEM_H_
#define MBED_FILESYSTEM_H_

#include <stdint.h>
#include <stdio.h>

#ifdef  __cplusplus
extern "C" {
#endif

struct __FILE {
  uint32_t handle;
  uint32_t offset;
};

#define O_RDONLY                0x0000
#define O_WRONLY                0x0001
#define O_RDWR                  0x0002
#define O_TRUNC                 0x1000
#define O_APPEND                0x2000

int32_t mbed_fs_open(FILE *fp, const char *filename, uint32_t flags) __attribute__((__nonnull__(1,2)));
int32_t mbed_fs_close(FILE *fp) __attribute__((__nonnull__(1)));

size_t mbed_fs_read(FILE *fp, char *buf, size_t count) __attribute__((__nonnull__(1,2)));
size_t mbed_fs_write(FILE *fp, const char *buf, size_t count) __attribute__((__nonnull__(1,2)));

int32_t mbed_fs_seek(FILE *fp, int32_t offset, uint32_t whence) __attribute__((__nonnull__(1)));

int32_t mbed_read_mac_address(uint8_t *mac) __attribute__((__nonnull__(1)));

#ifdef __cplusplus
}
#endif

#endif
